#!/usr/local/bin/python2

import jinja2
import os

src = open("wildcard.json.j2")
t = jinja2.Template(src.read())

out = t.render(
            external_ip=os.getenv('EXTERNAL_IP'),
            route53_zone_id=os.getenv('ROUTE53_ZONEID'),
            wildcard_record_name=os.getenv('ROUTE53_WILDCARD_RECORD_NAME')
        )

print(out)