#!/bin/bash

set -e -o 'pipefail'
source bash_colors.sh

# variables
[ -f .env ] && source .env

# lets start
clr_blue "+ getting creds ..."
gcloud container clusters get-credentials "${CLUSTER_NAME}" \
    --zone "${GAZ}" \
    --project "${PROJECTID}"

clr_blue "+ starting kubectl proxy ..."
kubectl proxy >/dev/null 2>&1 </dev/null &

clr_blue "+ waiting for ingress external ip ..."
IP=""
while [[ -z "${IP}" ]]; do
    IP=$(kubectl get svc --namespace gitlab-managed-apps ingress-nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
    sleep 3
done
export EXTERNAL_IP=${IP}

clr_blue "+ updating route53 (${EXTERNAL_IP}) ..."
aws --profile "${AWS_PROFILE}" route53 change-resource-record-sets \
    --hosted-zone-id "${ROUTE53_ZONEID}" \
    --cli-input-json "$(./aws-route53-wildcard-batch.py)"
sleep 20

clr_blue "+ provisionning cluster monitoring with helm ..."
helm init --upgrade
sleep 30
if ! (helm ls --all | grep monitoring) >/dev/null; then
    helm install --namespace tools --name monitoring monitoring
fi

# clr_blueb "+ launching gitlab-runners with helm ..."
# helm repo add gitlab https://charts.gitlab.io
# clr_cyan "  - gitlab runner 1"
# helm install --name "gitlab-runner-1" --namespace "gitlab-runners" \
#     --set runnerRegistrationToken="${GITLAB_REGISTRATION_TOKEN}"   \
#     -f gitlab-runner-values.yml gitlab/gitlab-runner
# helm install --name "gitlab-runner-2" --namespace "gitlab-runners" \
#     --set runnerRegistrationToken="${GITLAB_REGISTRATION_TOKEN}"   \
#     -f gitlab-runner-values.yml gitlab/gitlab-runner

clr_green "\o/ Groovy Baby \o/ !!!"