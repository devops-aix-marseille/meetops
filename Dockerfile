FROM golang:1.9.2

# install redis-server
RUN apt-get update -yqq && \
    apt-get install -yqq redis-server jq lsof && \
    rm -rf /var/lib/apt/lists/*

# deploy the app
COPY ./src/kaabot /go/src/kaabot
COPY vendor /go/vendor
COPY ./docker/entrypoint.sh /
COPY ./data /data
WORKDIR /go/src/kaabot

# build our app
RUN go-wrapper download github.com/constabulary/gb/...
RUN go-wrapper install github.com/constabulary/gb/...
RUN gb vendor restore
RUN gb build all

CMD  [ "/bin/bash", "/entrypoint.sh" ]

EXPOSE 8080
EXPOSE 9102

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD [ "curl", "-f", "http://localhost:8080/health" ]

# keep it at the end to avoid rebuilds
ENV KAABOT_REDIS_HOST=localhost
ENV KAABOT_REDIS_PORT=6379
ENV KAABOT_REDIS_PASSWORD=
ENV KAABOT_REDIS_DB=0
ENV KAABOT_TG_TOKEN=xxxxxxxxx:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#ENV GODEBUG=netdns=cgo+1