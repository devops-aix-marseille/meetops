#!env python

from lxml import html, etree
import simplejson as json

from pprint import pprint

hparser = etree.HTMLParser(encoding='utf-8')
tree   = etree.parse('data/kaamelott_quotes.htm', hparser)

# personnage: <h3><span class="mw-headline" id="Appius_Manilius"><a href="https://fr.wikipedia.org/wiki/Personnages_de_Kaamelott#Appius_Manilius" class="extiw" title="w:Personnages de Kaamelott">Appius Manilius</a></span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Kaamelott&amp;action=edit&amp;section=4" title="Modifier la section : Appius Manilius">modifier</a><span class="mw-editsection-bracket">]</span></span></h3>
# quote     : <p><span class="citation">

output = dict()

# This will create a list of quotes

quotes = tree.xpath('//span[@class="citation"]/text()')
#print("- quotes: ")
#pprint(quotes, indent=2, width=80, depth=5)

print(json.dumps(quotes, ensure_ascii=True, indent=4))