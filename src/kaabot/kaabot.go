package main

import (
	JSON "encoding/json"
	"fmt"
	"github.com/armon/go-metrics"
	"github.com/armon/go-metrics/prometheus"
	redis "github.com/go-redis/redis"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	gonfig "github.com/webofmars/gonfig"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"
)

// GetRandIndice : returns a valid indice in an array of string
func GetRandIndice(a []string) int {
	// produce a pseudo-random number between 0 and len(a)-1
	i := int(float32(len(a)) * rand.Float32())
	return i
}

// GetRandQuote : load a random quote from list
func GetRandQuote(a []string, i int) string {
	return a[i]
}

// LoadKaamelotQuotes : load our quotes from DB
func LoadKaamelotQuotes(redis *redis.Client) []string {
	var quotes []string
	var err error

	val, err := redis.Get("kaaquotes").Result()
	if err != nil {
		log.Fatalf("Can't load redis data : %e\n", err)
	}

	err = JSON.Unmarshal([]byte(val), &quotes)
	if err != nil {
		log.Fatalf("Can't unmarshal JSON data : %v\n", err)
	}
	log.Print("quotes succefully loaded !")
	return quotes
}

// GetConfigFromEnv : Load configuration from env values
func GetConfigFromEnv() gonfig.Config {

	conf := gonfig.NewConfig(nil)

	// set the defaults
	conf.Defaults.Reset(map[string]string{
		"REDIS_HOST":     "redis",
		"REDIS_PORT":     "6379",
		"REDIS_PASSWORD": "",
		"REDIS_DB":       "0",
		"TG_TOKEN":       "",
	})

	conf.Use("env", gonfig.NewEnvConfig("KAABOT_"))

	return conf

}

// OnRedisConnect : Callback when redis is connected
func OnRedisConnect(*redis.Conn) {
	log.Println("redis is connected ! bastard !")
}

// HttpStatusHandler : expose status for healthcheck
func HttpStatusHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "{ \"status\" : \"healthy\" }")
	log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.RequestURI)
}

// StartHealthAPIServer : starts the health api server
func StartHealthAPIServer() {
	log.Printf("HTTP health API started")
	http.HandleFunc("/health", HttpStatusHandler)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("HTTP: ListenAndServe: ", err)
	}
}

func StartPrometheusExporter() {
	log.Printf("prometheus exporter starting")
	http.Handle("/metrics", promhttp.Handler())
	err := http.ListenAndServe(":9102", nil)
	if err != nil {
		log.Fatal("HTTP: ListenAndServe: ", err)
	}
	log.Printf("prometheus exporter started")
}

func main() {
	var err error

	// seed the random generator
	rand.Seed(time.Now().UTC().UnixNano())

	// get the current hostname
	me, _ := os.Hostname()

	// get config
	conf := GetConfigFromEnv()

	redisHost := conf.Get("REDIS_HOST")
	redisPort := conf.Get("REDIS_PORT")
	redisPass := conf.Get("REDIS_PASSWORD")
	redisDb, err := strconv.Atoi(conf.Get("REDIS_DB"))

	// start metrics sync
	sink, err := prometheus.NewPrometheusSink()
	if err != nil {
		log.Fatalf("Can't start prometheus sync: %v", err)
	}
	log.Printf("sucessfully started prometheus sink")
	metrics.NewGlobal(metrics.DefaultConfig("kaabot"), sink)
	go StartPrometheusExporter()

	// Connect to redis backend
	if err != nil {
		log.Fatalf("Cant get redis database : %v", err)
	}
	ip, _ := net.LookupHost(redisHost)

	log.Printf("redis addr is %v (%v)", redisHost, ip)
	log.Printf("redis port is %v", redisPort)
	log.Printf("redis password is %v", redisPass)
	log.Printf("redis db is %v", redisDb)
	log.Printf("DSN to redis: redis://:%v@%v:%v/%d\n", redisPass, redisHost, redisPort, redisDb)

	// connect to redis
	client := redis.NewClient(&redis.Options{
		Addr:     redisHost + ":" + redisPort,
		Password: "",
		DB:       0,
		OnConnect: func(cn *redis.Conn) error {
			log.Println("Redis Rock n Roll !")
			return cn.ClientSetName("on_connect").Err()
		},
	})

	pong, err := client.Ping().Result()
	fmt.Printf("redis> %v : %v\n", pong, err)

	// load our quotes from redis
	quotes := LoadKaamelotQuotes(client)

	// starts health API server
	go StartHealthAPIServer()

	// starts looking for telegram messages
	// connect to telegram
	bot, err := tgbotapi.NewBotAPI(conf.Get("TG_TOKEN"))
	if err != nil {
		log.Panicf("Can't connect with telegram API : %v (%s)", err, conf.Get("TG_TOKEN"))
	}

	bot.Debug = true
	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	for update := range updates {
		if update.Message == nil {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

		i := GetRandIndice(quotes)
		quote := GetRandQuote(quotes, i)

		// metrics.IncrCounter([]string{"user", strconv.Itoa(update.Message.From.ID)}, 1)
		// metrics.IncrCounter([]string{"quote", strconv.Itoa(i)}, 1)
		metrics.IncrCounterWithLabels([]string{"quotes_total"}, 1,
			[]metrics.Label{
				metrics.Label{"id", strconv.Itoa(i)},
				metrics.Label{"user", strconv.Itoa(update.Message.From.ID)},
			})

		msg := tgbotapi.NewMessage(
			update.Message.Chat.ID,
			fmt.Sprintf("%v (from %s)", quote, me),
		)

		bot.Send(msg)
	}

}
