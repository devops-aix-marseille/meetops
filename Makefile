BUILDDIR=$(abspath .)
SOURCEDIR=$(abspath .)
GOPATH=$(HOME)/go

all: clean getdeps tests containers

clean:
	rm -rf $(SOURCEDIR)/bin/*
	rm -rf $(SOURCEDIR)/pkg/*
	rm -rf $(SOURCEDIR)/vendor/src/*
	rm -rf $(SOURCEDIR)/.cache/*
	go clean

getdeps:
	$(GOPATH)/bin/gb vendor restore

tests: tests-go tests-herokuish

tests-go:
	$(GOPATH)/bin/gb test

tests-herokuish:
	docker run --rm -v $(SOURCEDIR):/tmp/app \
	-v $(BUILDDIR)/.cache:/tmp/cache \
	gliderlabs/herokuish /bin/herokuish buildpack test

containers: tests-go
	docker build -t webofmars/kaabot:develop $(BUILDDIR)

minikube-containers:
	eval $(minikube docker-env)
	docker build -t webofmars/kaabot:develop $(BUILDDIR)

minikube-deploy:
	cd $(BUILDIR)
	helm init --client-only
	helm dependency update chart
	helm dependency build chart
	helm install -f chart/values-minikube.yaml --name kaabot --namespace develop chart

minikube-delete:
	cd $(BUILDIR)
	helm init --client-only
	helm delete --purge develop
	kubectl delete namespace develop